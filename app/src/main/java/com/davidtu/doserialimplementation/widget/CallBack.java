package com.davidtu.doserialimplementation.widget;

/**
 * Created by davidtu on 21/12/2017.
 */

public interface CallBack<T> {

    public void call(T result);
}
