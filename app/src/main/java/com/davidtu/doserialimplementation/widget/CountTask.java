package com.davidtu.doserialimplementation.widget;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by davidtu on 21/12/2017.
 */

public class CountTask implements Runnable {

    Logger logger = LoggerFactory.getLogger(CountTask.class);


    CallBack callBack;

    public void setCallBack(CallBack callBack) {
        this.callBack = callBack;
    }

    @Override
    public void run() {
        for (int count = 0; count <= 100; count++) {
            try {
                Thread.sleep(1);
                if (callBack != null) {
                    logger.debug("count:" + count);
                    callBack.call(count);
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

    }

}
