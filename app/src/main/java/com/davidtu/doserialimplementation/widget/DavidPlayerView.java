package com.davidtu.doserialimplementation.widget;

/**
 * Created by davidtu on 14/12/2017.
 */

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;

/**
 * Created by louis1chen on 12/10/2016.
 */

public class DavidPlayerView extends View {
    private float borderWidth = 1f; //dp
    private float margin = 5f;
    private final int MIN_WIDTH_DP = 200; //Can be adjust
    private final int MIN_HEIGHT_DP = 40; //Can be adjust\
    private String url;
    Rect border;
    Rect main;
    Rect progress;
    RectF controllerCircle;
    Paint borderPainter;
    Paint mainPainter;
    Paint circlePainter;
    Paint progressPainter;
    float currentPercent;


    public DavidPlayerView(Context context) {
        super(context);
        init();
        initDraw();
    }

    public DavidPlayerView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
        initDraw();
    }

    public DavidPlayerView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
        initDraw();
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUrl() {
        return url;
    }

    private void init() {
    }

    public void setPercent(float percent) {
        currentPercent = percent;
        invalidate();
    }

    private void initDraw() {
        borderPainter = new Paint();
        borderPainter.setColor(Color.RED);

        mainPainter = new Paint();
        mainPainter.setColor(Color.BLUE);

        circlePainter = new Paint();
        circlePainter.setAntiAlias(true);
        circlePainter.setColor(Color.WHITE);

        progressPainter = new Paint();
        progressPainter.setColor(Color.GRAY);

        controllerCircle = new RectF();
        border = new Rect();
        main = new Rect();
        progress = new Rect();

        int distanceMargin = (int) dp2px(getResources(), margin);
        int distanceBorder = (int) dp2px(getResources(), borderWidth);
        border.set(distanceMargin, distanceMargin, getWidth() - distanceMargin, getHeight() - distanceMargin);
        main.set(border.left + distanceBorder, border.top + distanceBorder, border.right - distanceBorder, border.bottom - distanceBorder);

        progress.set(main.left, main.top, main.left + (int) (main.width() / 100f * currentPercent), main.bottom);


        int circleLength = main.height() * 3 / 4;
        int distanceBetweenTopBottom = (main.height() - circleLength) / 2;
        int circleLeft = main.left + getWidth() / 16;
//        int padding

        controllerCircle.set(circleLeft, main.top + distanceBetweenTopBottom, circleLeft + circleLength, main.bottom - distanceBetweenTopBottom);

//        controllerCircle.centerY()


    }


    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        setMeasuredDimension(measure(widthMeasureSpec, true), measure(heightMeasureSpec, false));
    }

    private int measure(int measureSpec, boolean width) {
        int result;
        int mode = MeasureSpec.getMode(measureSpec);
        int size = MeasureSpec.getSize(measureSpec);
        if (mode == MeasureSpec.EXACTLY) {
            result = size;
        } else {
            result = (int) dp2px(getResources(), width ? MIN_WIDTH_DP : MIN_HEIGHT_DP);
            if (mode == MeasureSpec.AT_MOST) {
                result = Math.min(result, size);
            }
        }
        return result;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        initDraw();
        canvas.drawRect(border, borderPainter);
        canvas.drawRect(main, mainPainter);
        canvas.drawRect(progress, progressPainter);
        canvas.drawOval(controllerCircle, circlePainter);
    }

    protected float dp2px(Resources resources, float dp) {
        final float scale = resources.getDisplayMetrics().density;
        return dp * scale + 0.5f;
    }


    public enum BtnStatus {
        PLAY, STOP
    }

}

