package com.davidtu.doserialimplementation.utils;

import android.app.Activity;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;

import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;

/**
 * Created by louis1chen on 13/11/2017.
 */

@EActivity
public class PermissionAlertActivity extends Activity {
    @Extra
    protected PermissionCheckUtil.PermissionType[] permissionTypes;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState, @Nullable PersistableBundle persistentState) {
        super.onCreate(savedInstanceState, persistentState);
    }

    @AfterInject
    protected void afterInject() {
        PermissionCheckUtil.checkWithAlert(this, callback, permissionTypes);
    }

    private PermissionCheckUtil.PermissionAlertDialogCallback callback = new PermissionCheckUtil.PermissionAlertDialogCallback() {
        @Override
        public void onConfirmClicked() {

        }

        @Override
        public void onCancelClicked() {
            finish();
        }

        @Override
        public void onDismissed() {
            finish();
        }
    };
}
