package com.davidtu.doserialimplementation.utils;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v4.content.PermissionChecker;
import android.view.View;


import com.davidtu.doserialimplementation.application.MyApplication_;

import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Locale;

/**
 * Util for checking permission is allowed and popup alert
 * <p>
 * Created by louis1chen on 06/06/2017.
 */

@SuppressWarnings({"WeakerAccess", "unused"})
@EBean
public class PermissionCheckUtil {
    private static final Logger logger = LoggerFactory.getLogger(PermissionCheckUtil.class);

    @RootContext
    Context context;

    public enum PermissionType {
        ACCESS_CALENDAR("NEED ACCESS_CALENDAR", Manifest.permission.READ_CALENDAR, Manifest.permission.WRITE_CALENDAR),
        ACCESS_CAMERA("NEED ACCESS_CAMERA", Manifest.permission.CAMERA),
        ACCESS_STORAGE("NEED ACCESS_STORAGE", Manifest.permission.READ_EXTERNAL_STORAGE),
        ACCESS_CONTACTS("NEED ACCESS_CONTACTS", Manifest.permission.READ_CONTACTS),
        ACCESS_LOCATION("NEED ACCESS_LOCATION", Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION),
        RECORD_AUDIO("NEED RECORD_AUDIO", Manifest.permission.RECORD_AUDIO);

        private final String[] permissions;
        private final String alertMsg;

        private String getAlertMsg() {
            return alertMsg;
        }

        PermissionType(String text, String... permissions) {
            this.permissions = permissions;
            this.alertMsg = text;
        }



        private String[] getPermissions() {
            return permissions;
        }
    }

    /* Check permission */

    /**
     * Check the permissions are allowed.
     *
     * @param permissions The permissions to check, should be "Manifest.permission.[the permission you want to check]", ex: {@link Manifest.permission#RECORD_AUDIO}.
     * @return True if all permissions are allowed.
     */
    public static boolean checkPermission(String... permissions) {
        for (String permission : permissions) {
            boolean result = checkPermission(permission);
            if (!result) {
                return false;
            }
        }
        return true;
    }

    /**
     * Check the permission is allowed.
     *
     * @param permission The permission to check, should be "Manifest.permission.[the permission you want to check]", ex: {@link Manifest.permission#RECORD_AUDIO}.
     * @return True if permission is allowed.
     */
    public static boolean checkPermission(String permission) {
        return _checkPermission(permission);
    }

    /**
     * Check the permission is allowed.
     *
     * @param type The permission type to check
     * @return True if permission is allowed.
     */
    public static boolean checkPermission(PermissionType type) {
        return checkPermission(type.getPermissions());
    }

    /**
     * Check the permissions ard allowed.
     *
     * @param types The permission types to check
     * @return True if permissions are all allowed.
     */
    public static boolean checkPermission(PermissionType... types) {
        for (PermissionType type : types) {
            if (!checkPermission(type)) {
                return false;
            }
        }
        return true;
    }

    private static boolean _checkPermission(String permission) {
        int result = PermissionChecker.checkSelfPermission(MyApplication_.getInstance(), permission);
        logger.debug(String.format(Locale.getDefault(), "Check permission for [%s], result: %d", permission, result));
        return result == PermissionChecker.PERMISSION_GRANTED;
    }

    /* With alert */

    /**
     * Check the permission is allowed or popup an alert
     *
     * @param context Activity for alert dialog showing
     * @param types   The permission to check
     * @return True if permission is allowed.
     */
    public static boolean checkWithAlert(Context context, PermissionType... types) {
        for (PermissionType type : types) {
            if (!checkWithAlert(context, type.getAlertMsg(), type.getPermissions())) {
                return false;
            }
        }
        return true;
    }

    /**
     * Check the permission is allowed or popup an alert
     *
     * @param context Activity for alert dialog showing
     * @param types   The permission to check
     * @return True if permission is allowed.
     */
    public static boolean checkWithAlert(Context context, @Nullable PermissionAlertDialogCallback callback, PermissionType... types) {
        for (PermissionType type : types) {
            if (!checkWithAlert(context, type.getAlertMsg(), callback, type.getPermissions())) {
                return false;
            }
        }
        return true;
    }

    /**
     * Check the permission is allowed or popup an alert
     *
     * @param context     Activity for alert dialog showing
     * @param alertMsg    The message to show in the alert dialog content
     * @param permissions The permissions to check
     * @return True if all permissions is allowed.
     */
    public static boolean checkWithAlert(Context context, String alertMsg, String... permissions) {
        boolean result = checkPermission(permissions);
        if (!result) {
            showNonePermissionDialog(context, alertMsg, null);
        }
        return result;
    }

    /**
     * Check the permission is allowed or popup an alert
     *
     * @param context     Activity for alert dialog showing
     * @param alertMsg    The message to show in the alert dialog content
     * @param permissions The permissions to check
     * @return True if all permissions is allowed.
     */
    public static boolean checkWithAlert(Context context, String alertMsg, @Nullable PermissionAlertDialogCallback callback, String... permissions) {
        boolean result = checkPermission(permissions);
        if (!result) {
            showNonePermissionDialog(context, alertMsg, callback);
        }
        return result;
    }

    /**
     * Show a alert for non-permission
     *
     * @param context Activity for alert dialog showing
     */
    public static void showNonePermissionDialog(final Context context,String alertMsg ,@Nullable final PermissionAlertDialogCallback callback) {
        AlertDialog.Builder alert = new AlertDialog.Builder(context);
        alert.setTitle(alertMsg);
        alert.setPositiveButton(alertMsg,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Intent intent = new Intent();
                        intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                        Uri uri = Uri.fromParts("package", context.getPackageName(), null);
                        intent.setData(uri);
                        context.startActivity(intent);
                        dialogInterface.dismiss();
                    }
                });

        alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });

        Dialog dialog = alert.create();
        dialog.show();

    }

    public interface PermissionAlertDialogCallback {
        void onConfirmClicked();

        void onCancelClicked();

        void onDismissed();
    }

    /* With new page alert */

    public static boolean checkWithNewActivityAlert(Context context, PermissionType... types) {
        if (!checkPermission(types)) {
            PermissionAlertActivity_.intent(context).permissionTypes(types).flags(Intent.FLAG_ACTIVITY_NEW_TASK).start();
            return false;
        } else {
            return true;
        }
    }
}
