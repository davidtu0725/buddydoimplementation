package com.davidtu.doserialimplementation;

import android.os.Handler;

import com.davidtu.doserialimplementation.widget.CallBack;
import com.davidtu.doserialimplementation.widget.CountTask;
import com.davidtu.doserialimplementation.widget.DavidPlayerView;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by davidtu on 28/12/2017.
 */

public class CustomViewActivity {

    Handler handler;

    DavidPlayerView playerView;

    private void testCustomView(){
        ExecutorService singleThread = Executors.newSingleThreadExecutor();

        for (int i = 0; i < 100; i++) {
            CountTask task = new CountTask();
            task.setCallBack(countCallback);
            singleThread.submit(task);
        }
    }

    CallBack countCallback = new CallBack<Integer>() {

        @Override
        public void call(final Integer result) {
            handler.post(new Runnable() {
                @Override
                public void run() {
                    playerView.setPercent((float) result);

                }
            });
        }
    };
}
