package com.davidtu.doserialimplementation.contacts;

/**
 * Created by davidtu on 12/12/2017.
 */

public class BDDPostalAddress {

    private PostalAddressContactType type;

    private String value;

    public BDDPostalAddress(String address, PostalAddressContactType type) {
        value = address;
        this.type = type;
    }

    public PostalAddressContactType getType() {
        return type;
    }

    public String getValue() {
        return value;
    }

}
