package com.davidtu.doserialimplementation.contacts;

import android.provider.ContactsContract.CommonDataKinds.Email;

/**
 * Created by davidtu on 30/11/2017.
 */

public enum EmailContactType implements ContactType {
    TYPE_CUSTOM(Email.TYPE_CUSTOM, "TYPE_CUSTOM"), TYPE_HOME(Email.TYPE_HOME, "TYPE_HOME"),
    TYPE_WORK(Email.TYPE_WORK, "TYPE_WORK"), TYPE_OTHER(Email.TYPE_OTHER, "TYPE_OTHER"),
    TYPE_MOBILE(Email.TYPE_MOBILE, "TYPE_MOBILE");

    private final int index;
    private final String type;

    EmailContactType(int index, String type) {
        this.index = index;
        this.type = type;
    }

    public static EmailContactType getEnum(int value) {
        switch (value) {
            case Email.TYPE_CUSTOM:
                return TYPE_CUSTOM;
            case Email.TYPE_HOME:
                return TYPE_HOME;
            case Email.TYPE_WORK:
                return TYPE_WORK;
            case Email.TYPE_OTHER:
                return TYPE_OTHER;
            case Email.TYPE_MOBILE:
                return TYPE_MOBILE;
            default:
                return TYPE_OTHER;
        }
    }

    public String getString() {
        return type;
    }

}
