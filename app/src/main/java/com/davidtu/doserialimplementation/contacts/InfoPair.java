package com.davidtu.doserialimplementation.contacts;

/**
 * Created by davidtu on 30/11/2017.
 */

import java.io.Serializable;

/**
 * pair for (PhoneNumber|Email) with ContentType
 * eg.  0912-345-678 / FOR_WORK
 */
public class InfoPair implements Serializable {

    ContactType type;
    String data;

    public InfoPair(String data, ContactType type) {
        this.data = data;
        this.type = type;
    }

    public String getValue() {
        return data;
    }

    public ContactType getType() {
        return type;
    }
}
