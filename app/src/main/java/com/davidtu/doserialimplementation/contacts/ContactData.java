package com.davidtu.doserialimplementation.contacts;

import android.os.Bundle;
import android.support.annotation.NonNull;

import com.google.common.base.Strings;
import com.google.common.collect.ComparisonChain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by davidtu on 30/11/2017.
 */

public class ContactData implements Comparable<ContactData>, Serializable {
    public static String CONTACT_DATA = "CONTACT_DATA";
    private Long contactId;
    /**
     * struct name
     */
    private String dispName;
    private String firstName;
    private String middleName;
    private String lastName;
    /**
     * organization
     */
    private String company;
    private String title;

    private String photoUrl;
    private final String EMPTY = "";
    private List<BDDPhone> phone = new ArrayList<>();
    private List<BDDEmail> email = new ArrayList<>();



    private List<BDDPostalAddress> postalAddress = new ArrayList<>();


    public String getFirstName() {
        if (Strings.isNullOrEmpty(firstName)) {
            return EMPTY;
        } else {
            return firstName;
        }
    }

    public String getMiddleName() {
        if (Strings.isNullOrEmpty(middleName)) {
            return EMPTY;
        } else {
            return middleName;
        }
    }

    public String getLastName() {
        if (Strings.isNullOrEmpty(lastName)) {
            return EMPTY;
        } else {
            return lastName;
        }
    }

    public String getDispName() {
        if (Strings.isNullOrEmpty(dispName)) {
            return EMPTY;
        } else {
            return dispName;
        }
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    @Override
    public int compareTo(@NonNull ContactData contactData) {
        return ComparisonChain.start()
                .compare(getDispName(), contactData.getDispName(), ComparisonUtils.ALPHABETICAL_COMPARATOR)
                .result();
    }

    public void setDispName(String name) {
        this.dispName = name;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<BDDPhone> getPhone() {
        return phone;
    }

    public List<BDDEmail> getEmail() {
        return email;
    }

    public List<BDDPostalAddress> getPostalAddress() {
        return postalAddress;
    }

    /**
     * eg: return [a@truetel.com,b@truetel.com...]
     */
    public List<String> getPureEmail() {
        List<String> pure = new ArrayList<>();
        for (BDDEmail e : email) {
            if (e != null && !Strings.isNullOrEmpty(e.getValue())) {
                pure.add(e.getValue());
            }
        }
        return pure;
    }

    /**
     * eg: return [0912345678,0922345678...]
     */
    public List<String> getPurePhone() {
        List<String> pure = new ArrayList<>();
        for (BDDPhone p : phone) {
            if (p != null && !Strings.isNullOrEmpty(p.getValue())) {
                pure.add(p.getValue());
            }
        }
        return pure;
    }

    public void addEmail(String email, ContactType type) {
        BDDEmail bddEmail = new BDDEmail(email, (EmailContactType) type);
        this.email.add(bddEmail);
    }

    public void addPhone(String number, ContactType type) {
        BDDPhone bddPhone = new BDDPhone(number, (PhoneContactType) type);
        this.phone.add(bddPhone);
    }

    public void addPostalAddress(String address, ContactType type) {
        BDDPostalAddress bddAddress = new BDDPostalAddress(address, (PostalAddressContactType) type);
        this.postalAddress.add(bddAddress);
    }

    public List<String> getFlatStruct() {
        List<String> flat = new ArrayList<>();
        for (BDDPhone p : phone) {
            flat.add(p.toString());
        }
        for (BDDEmail e : email) {
            flat.add(e.getValue());
        }
        return flat;
    }

    public static List<ContactData> parse(Bundle bundle) {
        List<ContactData> data = new ArrayList<>();
        if (bundle != null) {
            try {
                data = (List<ContactData>) bundle.getSerializable(CONTACT_DATA);
                data = (data == null ? new ArrayList<ContactData>() : data);
            } catch (ClassCastException e) {
                /** avoid cast error */
                data = new ArrayList<>();
            }
        }
        return data;
    }

    public String toDebugString() {
        StringBuilder builder = new StringBuilder();
        builder.append(getDispName() == null ? "" : getDispName());
        builder.append("\n");
        builder.append(" photoUrl ").append(photoUrl);
        builder.append("\n");
        for (BDDPhone p : phone) {
            builder.append(" phone::: ").append(p.getValue().concat(" phone type: ").concat(p.getType().getString()));
            builder.append("\n");
        }
        for (BDDEmail p : email) {
            builder.append(" email::: ").append(p.getValue().concat(" email type: ").concat(p.getType().getString()));
            builder.append("\n");
        }
        builder.append("\n");
        return builder.toString();
    }

    public Long getContactId() {
        return contactId;
    }

    public void setContactId(Long contactId) {
        this.contactId = contactId;
    }
}
