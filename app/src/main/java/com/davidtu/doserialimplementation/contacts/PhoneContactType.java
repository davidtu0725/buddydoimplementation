package com.davidtu.doserialimplementation.contacts;

import android.provider.ContactsContract.CommonDataKinds.Phone;

/**
 * Created by davidtu on 30/11/2017.
 */

public enum PhoneContactType implements ContactType {

    TYPE_CUSTOM(Phone.TYPE_CUSTOM, "TYPE_CUSTOM"), TYPE_HOME(Phone.TYPE_HOME, "TYPE_HOME"), TYPE_MOBILE(Phone.TYPE_MOBILE, "TYPE_MOBILE"),
    TYPE_WORK(Phone.TYPE_WORK, "TYPE_WORK"), TYPE_FAX_WORK(Phone.TYPE_FAX_WORK, "TYPE_FAX_WORK"),
    TYPE_FAX_HOME(Phone.TYPE_FAX_HOME, "TYPE_FAX_HOME"), TYPE_PAGER(Phone.TYPE_PAGER, "TYPE_PAGER"),
    TYPE_OTHER(Phone.TYPE_OTHER, "TYPE_OTHER");


    private final int index;
    private final String type;

    PhoneContactType(int index, String type) {
        this.index = index;
        this.type = type;
    }

    public static PhoneContactType getEnum(int value) {
        switch (value) {
            case Phone.TYPE_CUSTOM:
                return TYPE_CUSTOM;
            case Phone.TYPE_HOME:
                return TYPE_HOME;
            case Phone.TYPE_MOBILE:
                return TYPE_MOBILE;
            case Phone.TYPE_WORK:
                return TYPE_WORK;
            case Phone.TYPE_FAX_WORK:
                return TYPE_FAX_WORK;
            case Phone.TYPE_FAX_HOME:
                return TYPE_FAX_HOME;
            case Phone.TYPE_PAGER:
                return TYPE_PAGER;
            case Phone.TYPE_OTHER:
                return TYPE_OTHER;
            default:
                return TYPE_OTHER;
        }
    }

    public String getString() {
        return type;
    }

    /**
     * avoid some case of custmized situation
     */
    public static PhoneContactType getPhoneContactTypeEnum(int type) {
        if (type > 4 || type < 1) {
            return PhoneContactType.getEnum(0);
        }
        return PhoneContactType.getEnum(type);
    }

}
