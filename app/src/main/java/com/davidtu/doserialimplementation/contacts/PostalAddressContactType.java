package com.davidtu.doserialimplementation.contacts;

import android.provider.ContactsContract.CommonDataKinds.StructuredPostal;

/**
 * Created by davidtu on 30/11/2017.
 */

public enum PostalAddressContactType implements ContactType {
    TYPE_CUSTOM(StructuredPostal.TYPE_CUSTOM, "TYPE_CUSTOM"), TYPE_HOME(StructuredPostal.TYPE_HOME, "TYPE_HOME"),
    TYPE_WORK(StructuredPostal.TYPE_WORK, "TYPE_WORK"), TYPE_OTHER(StructuredPostal.TYPE_OTHER, "TYPE_OTHER");

    private final int index;
    private final String type;

    PostalAddressContactType(int index, String type) {
        this.index = index;
        this.type = type;
    }

    public static PostalAddressContactType getEnum(int value) {
        switch (value) {
            case StructuredPostal.TYPE_CUSTOM:
                return TYPE_CUSTOM;
            case StructuredPostal.TYPE_HOME:
                return TYPE_HOME;
            case StructuredPostal.TYPE_WORK:
                return TYPE_WORK;
            case StructuredPostal.TYPE_OTHER:
            default:
                return TYPE_OTHER;

        }
    }

    public String getString() {
        return type;
    }

}
