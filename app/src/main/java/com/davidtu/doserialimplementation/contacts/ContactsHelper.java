package com.davidtu.doserialimplementation.contacts;

import android.content.ContentUris;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;
import android.util.Log;

import com.google.common.base.Strings;
import com.google.common.io.Files;

import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by davidtu on 29/11/2017.
 */

@EBean
public class ContactsHelper {

    @RootContext
    Context context;

    private static final String[] PROJECTION_CONTACT_PHONE = {
            ContactsContract.CommonDataKinds.Phone.CONTACT_ID,
            ContactsContract.CommonDataKinds.Phone.NUMBER,
            ContactsContract.CommonDataKinds.Phone.TYPE,
    };

    private static final String[] PROJECTION_CONTACT_EMAIL = {
            ContactsContract.CommonDataKinds.Email.CONTACT_ID,
            ContactsContract.CommonDataKinds.Email.DATA,
            ContactsContract.CommonDataKinds.Email.TYPE,
    };

    private static final String[] PROJECTION_CONTACT_ADDRESS = {
            ContactsContract.CommonDataKinds.StructuredPostal.CONTACT_ID,
            ContactsContract.CommonDataKinds.StructuredPostal.FORMATTED_ADDRESS,
            ContactsContract.CommonDataKinds.StructuredPostal.TYPE,
    };

    private static final String[] PROJECTION_ORGANIZATION = {
            ContactsContract.CommonDataKinds.Organization.CONTACT_ID,
            ContactsContract.CommonDataKinds.Organization.COMPANY,
            ContactsContract.CommonDataKinds.Organization.TITLE,
    };

    private static final String[] PROJECTION_CONTACT_NAME = {
            ContactsContract.CommonDataKinds.StructuredName.CONTACT_ID,
            ContactsContract.CommonDataKinds.StructuredName.DISPLAY_NAME,
            ContactsContract.CommonDataKinds.StructuredName.FAMILY_NAME,
            ContactsContract.CommonDataKinds.StructuredName.MIDDLE_NAME,
            ContactsContract.CommonDataKinds.StructuredName.GIVEN_NAME,
            ContactsContract.CommonDataKinds.StructuredName.FULL_NAME_STYLE
    };

    public class Name {
        public Long contactId;
        public String displayName;
        public String familyName;
        public String givenName;
        public String middleName;
        public int fullNameStyle;
        public int[] type = {
                ContactsContract.FullNameStyle.UNDEFINED,
                ContactsContract.FullNameStyle.WESTERN,
                ContactsContract.FullNameStyle.CJK,
                ContactsContract.FullNameStyle.CHINESE,
                ContactsContract.FullNameStyle.JAPANESE,
                ContactsContract.FullNameStyle.KOREAN};
    }

    public List<ContactData> getContacts() {
        long startTime = System.currentTimeMillis();
        Map<Long, ContactData> contactMap = new HashMap<>();
        Cursor cursor;
        /** fetch contacts numbers */
        cursor = context.getContentResolver().query(
                ContactsContract.CommonDataKinds.Phone.CONTENT_URI, PROJECTION_CONTACT_PHONE, null, null, null);
        try {
            if (cursor != null) {
                while (cursor.moveToNext()) {
                    Long contactId = cursor.getLong(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.CONTACT_ID));
                    String phoneNumber = cursor.getString(1);
                    int type = cursor.getInt(2);
                    phoneNumber = phoneNumber.replaceAll("(\\s|-)", "");
                    addPhone(contactMap, contactId, phoneNumber, PhoneContactType.getPhoneContactTypeEnum(type));
                }
            }
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }

        /** fetch contacts emails */
        cursor = context.getContentResolver().query(
                ContactsContract.CommonDataKinds.Email.CONTENT_URI, PROJECTION_CONTACT_EMAIL, null, null, null);
        try {
            if (cursor != null) {
                while (cursor.moveToNext()) {
                    Long contactId = cursor.getLong(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Email.CONTACT_ID));
                    String email = cursor.getString(1);
                    int type = cursor.getInt(2);
                    addEmail(contactMap, contactId, email, EmailContactType.getEnum(type));
                }
            }
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }

        /** fetch contacts postal address */
        cursor = context.getContentResolver().query(
                ContactsContract.CommonDataKinds.StructuredPostal.CONTENT_URI, PROJECTION_CONTACT_ADDRESS, null, null, null);
        try {
            if (cursor != null) {
                while (cursor.moveToNext()) {
                    Long contactId = cursor.getLong(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Email.CONTACT_ID));
                    String postal = cursor.getString(1);
                    int type = cursor.getInt(2);
                    addPostalAddress(contactMap, contactId, postal, PostalAddressContactType.getEnum(type));
                }
            }
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        /** fetch contacts organization */
        String orgWhere =
                ContactsContract.CommonDataKinds.Organization.COMPANY + " is not null " + " AND " +
                        ContactsContract.Data.MIMETYPE + " = ? ";
        String[] orgWhereParams = new String[]{
                ContactsContract.CommonDataKinds.Organization.CONTENT_ITEM_TYPE};
        cursor = context.getContentResolver().query(ContactsContract.Data.CONTENT_URI,
                PROJECTION_ORGANIZATION, orgWhere, orgWhereParams, null);

        try {
            if (cursor != null) {
                while (cursor.moveToNext()) {
                    Long contactId = cursor.getLong(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Email.CONTACT_ID));
                    String comany = cursor.getString(1);
                    String title = cursor.getString(2);
                    addComanyTitle(contactMap, contactId, comany, title);
                }
            }
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }


        /** fetch contacts name */
        Map<Long, Name> nameMap = new HashMap<>();
        String nameSelection = ContactsContract.CommonDataKinds.StructuredName.DISPLAY_NAME + " is not null "
                + " AND "
                + ContactsContract.CommonDataKinds.StructuredName.FULL_NAME_STYLE + " is not null ";
        cursor = context.getContentResolver().query(ContactsContract.Data.CONTENT_URI, PROJECTION_CONTACT_NAME, nameSelection, null, null);
        try {
            if (cursor != null) {
                while (cursor.moveToNext()) {
                    Long contactId = cursor.getLong(cursor.getColumnIndex(ContactsContract.CommonDataKinds.StructuredName.CONTACT_ID));
                    String disp_name = cursor.getString(1);
                    String family_name = cursor.getString(2);
                    String middle_name = cursor.getString(3);
                    String given_name = cursor.getString(4);
                    int full_name_style = cursor.getInt(5);


                    if (isValidByFullNameStyle(disp_name, family_name, middle_name, given_name)) {
                        Name name = new Name();
                        name.contactId = contactId;
                        name.familyName = family_name;
                        name.middleName = middle_name;
                        name.givenName = given_name;
                        name.displayName = disp_name;
                        name.fullNameStyle = full_name_style;

                        nameMap.put(contactId, name);
                    }
                }
            }
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        addName(contactMap, new ArrayList<>(nameMap.values()));
        List<ContactData> originContactList = new ArrayList<>(contactMap.values());
        originContactList = cleanUglyData(originContactList);
        Collections.sort(originContactList);
        /** filter Uri to absolute path */
        handlePhotoPath(originContactList);
        long endTime = System.currentTimeMillis();
        Log.d("debug", "total run time = " + Double.valueOf(endTime - startTime / 1000) + " 秒");

        return originContactList;

    }

    private List<ContactData> cleanUglyData(List<ContactData> originContactList) {
        /** table may contains ugly data */
        ArrayList<ContactData> pureContactData = new ArrayList<>();
        for (ContactData data : originContactList) {
            if (!Strings.isNullOrEmpty(data.getDispName())) {
                pureContactData.add(data);
            }
        }
        return pureContactData;
    }

    private boolean isValidByFullNameStyle(String full, String first, String middle, String last) {
        if (full == null) {
            return false;
        }
        /** nameStyle = /FamilyName/MiddleName/GivenName/ */
        String nameStyle = full.replace(" ", "");
        StringBuilder _eastPattern = new StringBuilder();
        _eastPattern.append(first == null ? "" : first.trim());
        _eastPattern.append(middle == null ? "" : middle.trim());
        _eastPattern.append(last == null ? "" : last.trim());
        String eastPattern = _eastPattern.toString().replace(" ", "");

        /** nameStyle = /GivenName/MiddleName/FamilyName/ */
        StringBuilder _westernPattern = new StringBuilder();
        _westernPattern.append(last == null ? "" : last.trim());
        _westernPattern.append(middle == null ? "" : middle.trim());
        _westernPattern.append(first == null ? "" : first.trim());
        String westernPattern = _westernPattern.toString().replace(" ", "");

        if (nameStyle.equals(eastPattern) || nameStyle.equals(westernPattern)) {
            return true;
        }
        return false;
    }

    private void handlePhotoPath(List<ContactData> originContactList) {
        for (ContactData data : originContactList) {
            String path = getThumbnailPath(context, data.getContactId());
            if (!Strings.isNullOrEmpty(path)) {
                data.setPhotoUrl(path);
            } else {
                data.setPhotoUrl(null);
            }
        }
    }

    private void addPhone(Map<Long, ContactData> map, Long id, String number, PhoneContactType type) {
        if (!Strings.isNullOrEmpty(number)) {
            InfoPair pair = new InfoPair(number, type);
            addRecord(map, id, pair, null, null);
        }
    }

    private void addEmail(Map<Long, ContactData> map, Long id, String email, EmailContactType type) {
        if (!Strings.isNullOrEmpty(email)) {
            InfoPair pair = new InfoPair(email, type);
            addRecord(map, id, null, pair, null);
        }
    }

    protected void addPostalAddress(Map<Long, ContactData> map, Long id, String address, PostalAddressContactType type) {
        if (!Strings.isNullOrEmpty(address)) {
            InfoPair pair = new InfoPair(address, type);
            addRecord(map, id, null, null, pair);
        }
    }

    private void addComanyTitle(Map<Long, ContactData> map, Long id, String comany, String title) {
        if (map.containsKey(id)) {
            ContactData data = map.get(id);
            data.setCompany(comany);
            data.setTitle(title);
        }
    }

    /**
     * In theory,the 'last name' equal to 'family name'
     * and the 'fist name' equal to 'given name'
     */
    protected void addName(Map<Long, ContactData> map, List<Name> list) {
        for (Name name : list) {
            String firstName = name.givenName;
            if (name.fullNameStyle == ContactsContract.FullNameStyle.CHINESE) {
                /** it may happen following sitatuation
                 * FamilyName = 王
                 * MiddleName = 小
                 * GivenNmae = 明
                 * so that we have to hack first_name
                 * */
                StringBuilder _firstName = new StringBuilder();
                _firstName.append(name.middleName == null ? "" : name.middleName);
                _firstName.append(name.givenName == null ? "" : name.givenName);
                firstName = _firstName.toString();
            }
            ContactData data = map.get(name.contactId);
            data = (data == null ? new ContactData() : data);
            data.setContactId(name.contactId);
            data.setDispName(name.displayName);
            data.setLastName(name.familyName);
            data.setFirstName(firstName);
            data.setMiddleName(name.middleName);
            map.put(name.contactId, data);
        }
    }

    private void addRecord(Map<Long, ContactData> contactMap, Long contactId, InfoPair phoneNumber, InfoPair email, InfoPair address) {
        if (contactMap == null || contactId == null) {
            return;
        }

        if (phoneNumber == null && email == null && address == null) {
            return;
        }

        ContactData data = contactMap.get(contactId);

        if (data == null) {
            data = new ContactData();
            data.setContactId(contactId);
            contactMap.put(contactId, data);
        }

        if (phoneNumber != null) {
            data.addPhone(phoneNumber.getValue(), phoneNumber.getType());
        }

        if (email != null) {
            data.addEmail(email.getValue(), email.getType());
        }

        if (address != null) {
            data.addPostalAddress(address.getValue(), address.getType());
        }
    }


    public String getThumbnailPath(Context c, long contactId) {
        Uri contactUri = ContentUris.withAppendedId(ContactsContract.Contacts.CONTENT_URI, contactId);
        Uri photoUri = Uri.withAppendedPath(contactUri, ContactsContract.Contacts.Photo.CONTENT_DIRECTORY);
        Cursor cursor = c.getContentResolver().query(photoUri,
                new String[]{ContactsContract.Contacts.Photo.PHOTO}, null, null, null);
        if (cursor == null) {
            return null;
        }
        try {
            if (cursor.moveToFirst()) {
                byte[] data = cursor.getBlob(0);
                if (data != null) {
                    File cache = File.createTempFile("cache_contacts".concat(String.valueOf(contactId)), ".jpg", c.getCacheDir());
                    InputStream inputStream = new ByteArrayInputStream(data);
                    byte[] buffer = new byte[inputStream.available()];
                    inputStream.read(buffer);
                    Files.write(buffer, cache);
                    return "file://" + cache.getAbsolutePath();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            cursor.close();
        }
        return null;
    }
}
