package com.davidtu.doserialimplementation.contacts;

/**
 * Created by davidtu on 30/11/2017.
 */

public interface ContactType {
    String getString();
}