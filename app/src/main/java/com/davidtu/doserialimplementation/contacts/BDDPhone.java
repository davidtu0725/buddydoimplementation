package com.davidtu.doserialimplementation.contacts;

import java.io.Serializable;

/**
 * Created by davidtu on 05/12/2017.
 */

public class BDDPhone implements Serializable {

    private PhoneContactType type;

    private String value;


    public BDDPhone(String phoneNum, PhoneContactType type) {
        value = phoneNum;
        this.type = type;
    }

    public PhoneContactType getType() {
        return type;
    }

    public String getValue(){
        return value;
    }

}
