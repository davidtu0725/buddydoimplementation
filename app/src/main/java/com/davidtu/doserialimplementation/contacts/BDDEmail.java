package com.davidtu.doserialimplementation.contacts;

import java.io.Serializable;

/**
 * Created by davidtu on 05/12/2017.
 */

public class BDDEmail implements Serializable {

    private EmailContactType type;

    private String value;

    public BDDEmail(String email, EmailContactType type) {
        value = email;
        this.type = type;
    }

    public EmailContactType getType() {
        return type;
    }

    public String getValue(){
        return value;
    }

}
