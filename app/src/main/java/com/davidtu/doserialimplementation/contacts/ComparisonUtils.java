package com.davidtu.doserialimplementation.contacts;

import com.google.common.collect.Ordering;

import java.text.Collator;
import java.util.Comparator;
import java.util.Date;
import java.util.Locale;

/**
 * @author David Tu
 */
public class ComparisonUtils {

    public static final Comparator<String> ALPHABETICAL_COMPARATOR = new AlphabeticalComparator();

    public static int compareAlphabetically(String str1, String str2) {
        if (str1 == null) {
            str1 = "";
        }
        if (str2 == null) {
            str2 = "";
        }
        return ALPHABETICAL_COMPARATOR.compare(str1, str2);
    }

    public static int compare(Date o1, Date o2) {
        if (o1 == null || o2 == null) {
            return 0;
        }
        return o1.compareTo(o2);
    }

    public static Ordering<String> localeAlphabeticalOrdering(Locale locale) {
        final Collator collator = Collator.getInstance(locale);
        Ordering<String> ordering = new Ordering<String>() {
            @Override
            public int compare(String left, String right) {
                int minLength = Math.min(left.length(), right.length());
                for (int i = 0; i < minLength; i++) {
                    String lhs = String.valueOf(left.charAt(i));
                    String rhs = String.valueOf(right.charAt(i));
                    int result = collator.compare(lhs.toLowerCase(), rhs.toLowerCase());
                    if (result != 0) {
                        return result;
                    }

                    result = collator.compare(lhs, rhs);

                    if (result != 0) {
                        return result;
                    }
                }
                return left.length() - right.length();
            }
        };
        return ordering.nullsFirst();
    }

    private static final class AlphabeticalComparator implements Comparator<String> {
        @Override
        public int compare(String lhs, String rhs) {
            int minLen = Math.min(lhs.length(), rhs.length());
            int res;
            for (int i = 0; i < minLen; i++) {
                Character c1 = lhs.toLowerCase().charAt(i);
                Character c2 = rhs.toLowerCase().charAt(i);
                Character loC1 = Character.toLowerCase(c1);
                Character loC2 = Character.toLowerCase(c2);
                res = loC1.compareTo(loC2);
                if (res != 0) {
                    return res;
                }
                res = c1.compareTo(c2);
                if (res != 0) {
                    return res;
                }
            }
            if (lhs.length() == rhs.length()) {
                return 0;
            }
            return (lhs.length() > rhs.length() ? 1 : -1);
        }
    }
}
