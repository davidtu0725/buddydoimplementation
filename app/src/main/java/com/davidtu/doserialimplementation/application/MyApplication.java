package com.davidtu.doserialimplementation.application;

import android.app.Application;

import org.androidannotations.annotations.EApplication;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.encoder.PatternLayoutEncoder;
import ch.qos.logback.classic.joran.JoranConfigurator;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.rolling.FixedWindowRollingPolicy;
import ch.qos.logback.core.rolling.RollingFileAppender;
import ch.qos.logback.core.rolling.SizeBasedTriggeringPolicy;

/**
 * Created by davidtu on 29/11/2017.
 */

@EApplication
public class MyApplication extends Application {

    Logger logger = LoggerFactory.getLogger(MyApplication.class);

    @Override
    public void onCreate() {
        super.onCreate();
        configureLogback();
    }

    public void configureLogback(){
        LoggerContext lc = (LoggerContext) LoggerFactory.getILoggerFactory();
        lc.reset();

        JoranConfigurator config = new JoranConfigurator();
        config.setContext(lc);

        try {
            // init logcat appender from config file
            InputStream fin = getResources().getAssets().open("logback.xml");
            config.doConfigure(fin);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        enableFileAppender();
    }

    private boolean isFileAppenderExists() {
        ch.qos.logback.classic.Logger root = (ch.qos.logback.classic.Logger) LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME);
        return root.getAppender("file-appender") != null;
    }

    public void enableFileAppender() {
        if (isFileAppenderExists()) {
            logger.debug("file appender already enabled");
            return;
        }

        LoggerContext lc = (LoggerContext) LoggerFactory.getILoggerFactory();

        File logFolder = null;
        try {
            logFolder = getLogFolder();
        } catch (IOException e) {
            logger.debug("fail to enable file appender", e);
        }

        RollingFileAppender<ILoggingEvent> fileAppender = new RollingFileAppender<>();
        fileAppender.setFile(new File(logFolder, "app.log").getAbsolutePath());

        FixedWindowRollingPolicy rollingPolicy = new FixedWindowRollingPolicy();
        rollingPolicy.setContext(lc);
        rollingPolicy.setParent(fileAppender);
        rollingPolicy.setMinIndex(1);
        rollingPolicy.setMaxIndex(9);
        rollingPolicy.setFileNamePattern(new File(logFolder, "davidtrylog.%i.log").getAbsolutePath());
        rollingPolicy.start();

        SizeBasedTriggeringPolicy<ILoggingEvent> triggerPolicy = new SizeBasedTriggeringPolicy<>();
        triggerPolicy.setContext(lc);
        triggerPolicy.setMaxFileSize("10MB");
        triggerPolicy.start();

        PatternLayoutEncoder encoder = new PatternLayoutEncoder();
        encoder.setContext(lc);
        encoder.setPattern("%d{HH:mm:ss.SSS} %.-1level/%logger{0}: [%thread] %msg%n");
        encoder.start();

        fileAppender.setContext(lc);
        fileAppender.setName("file-appender");
        fileAppender.setEncoder(encoder);
        fileAppender.setRollingPolicy(rollingPolicy);
        fileAppender.setTriggeringPolicy(triggerPolicy);
        fileAppender.setAppend(true);
        fileAppender.start();
        ch.qos.logback.classic.Logger root = (ch.qos.logback.classic.Logger) LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME);
        root.addAppender(fileAppender);
        logger.debug("file appender enabled, current time: " + new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault()).format(new Date()));
    }

    private File getLogFolder() throws IOException {
        File extCache = getExternalCacheDir();
        if (extCache == null) {
            throw new IOException("external storage not found");
        }

        return new File(extCache, "log");
    }

}
