package com.davidtu.doserialimplementation;

import android.os.Bundle;
import android.widget.Button;

import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;


@EActivity(resName = "activity_main")
public class MainActivity extends BaseActivity {

    @ViewById(resName = "btn_contacts")
    Button contacts;

    @ViewById(resName = "custom_view")
    Button customView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Click(resName = "btn_contacts")
    public void contactsActivity(){
        ContactActivity_.intent(this).start();

    }






}
