package com.davidtu.doserialimplementation;

import com.davidtu.doserialimplementation.contacts.ContactsHelper;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EActivity;

/**
 * Created by davidtu on 28/12/2017.
 */

@EActivity(resName = "layout_contacts")
public class ContactActivity extends BaseActivity {

    @Bean
    ContactsHelper helper;

    @AfterViews
    public void afterView(){

        helper.getContacts();

    }
}
